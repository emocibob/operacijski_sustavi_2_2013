# -*- coding: utf-8 -*-
import threading, time

zbroj = 0
zbroj_lock = threading.Lock()

def zbroj_ab(a, b):
    global zbroj
    zbroj_lock.acquire()
    for i in range(a, b + 1):
        zbroj += i
    zbroj_lock.release()

t1 = threading.Thread(target=zbroj_ab, args=(1, 250000))
t2 = threading.Thread(target=zbroj_ab, args=(250001, 500000))

print("Vrijednost var na pocetku:", zbroj)

t1.start()
t2.start()

print("Glavni program moze raditi sto zeli dok zbroj racuna.")

t1.join()
t2.join()

#print("t1", t1.is_alive())
#print("t2", t2.is_alive())

print("Zbroj na kraju:", zbroj)

zbroj2 = 0
for i in range(0, 500000 + 1):
    zbroj2 += i

print("zbroj2:", str(zbroj2))
