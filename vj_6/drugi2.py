# -*- coding: utf-8 -*-
import threading

var = 5
var_lock = threading.Lock()

def uvecaj_var_za(x):
    global var
    #var += x
    for a in range(0, x):
        var += 1
    var_lock.release()

t1 = threading.Thread(target=uvecaj_var_za, args=(100000,))
t2 = threading.Thread(target=uvecaj_var_za, args=(100000,))

print("Vrijednost var na pocetku:", var)

t1.start()
t2.start()

t1.join()
t2.join()

print("t1", t1.is_alive())
print("t2", t2.is_alive())

print("Vrijednost var na kraju:", var)

