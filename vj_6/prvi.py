import threading

def funkcija(x):
    print("Pokrenuta procesna nit.")
    print("Vrijednost x je", x)

t1 = threading.Thread(target=funkcija, args=(5,))
t2 = threading.Thread(target=funkcija, args=(7,))

t1.start()
t2.start()

t1.join()
t2.join()

print("Ovo je glavna procesna nit (glavni program)")

print("Izvodi li se jos uvijek t1?", t1.is_alive())
print("Izvodi li se jos uvijek t2?", t2.is_alive())
