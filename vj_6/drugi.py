# -*- coding: utf-8 -*-
import threading, time

def countdown_to_zero(n):
    print("Pokrecem odbrojavanje.")
    while n > 1:
        n -= 1
    print("Gotovo")

t1 = threading.Thread(target=countdown_to_zero, args=(651929250,))
t2 = threading.Thread(target=countdown_to_zero, args=(421858921,))
t3 = threading.Thread(target=countdown_to_zero, args=(2188312,))

t1.start()
t2.start()
t3.start()

time.sleep(5)
print("Spavao sa 5 s.")

if t1.is_alive():
    t1.join()

if t2.is_alive():
    t2.join()

if t3.is_alive():
    t3.join()
