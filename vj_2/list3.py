a=[1, "Bok", [24, 7, 365], ["Da", "Ne"]]

print(a, "\n")

print(a[1])
print(a[2])
print(a[2][0])
#print(a[2][4])
print(a[3][1])

print("")
a[0]=5
print(a)

print("")
a.insert(0, "Swedish House Mafia")
print(a)

print("")
a.pop()
a.append([5, 4, 2])
print(a)
