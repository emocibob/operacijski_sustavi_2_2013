import os
import time

pid = os.fork()

if pid == 0:
    os.execlp('ls', 'ls', '.')
else:
    time.sleep(5)

# echo $?      <- izlazni status zadnje pokrenute naredbe
