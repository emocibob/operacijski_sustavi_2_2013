import os
import time

pid = os.fork()

if pid == 0:
    # dijete
    os.setsid()
    pid2 = os.fork()
    if pid2 == 0:
        # dijete dijeteta
        print("Dijete je postalo demon.")
        time.sleep(5)
        print("Dijete je zavrsilo izvodjenje.")
    else:
        # dijete ili novi proces?
        exit()
else:
    # roditelj
    exit()
