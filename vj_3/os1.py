import os

print("PID: " + str(os.getpid()) + "\n" + "PPID: " + str(os.getppid()))
print("efektivni UID: " + str(os.geteuid()) + " i GID: " + str(os.getegid()))
print("stvarni UID: " + str(os.getuid()) + " i GID: " + str(os.getgid()))
print("dir: " + str(os.getcwd()))
print(str(os.listdir(os.getcwd())))
#path = "."
#print(str(os.listdir(path)))

# tty -> terminal
print(os.ttyname(0)) # std ulaz
print(os.ttyname(1)) # std izlaz
print(os.ttyname(2)) # std izlaz za greske
