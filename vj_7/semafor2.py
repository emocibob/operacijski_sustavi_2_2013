import threading
import time

semafor1 = threading.Semaphore(5)
knjiga1 = "Dostojevski: Z&K"
semafor2 = threading.Semaphore(3)
knjiga2 = "Tolstoj: AK"
semafor3 = threading.Semaphore(4)
knjiga3 = "Balzac: OG"

ucenici = ['Domagoj', 'Ivan', 'Luka', 'Snežana', 'Romana', 'Sonja', 'Marta']

def lektira(ime):
    print("Ja sam", ime, "i dosao/la sam u knjiznicu.")
    semafor1.acquire()
    print(ime, "cita", knjiga1, "i sara po margini.")
    semafor1.release()
    semafor2.acquire()
    print(ime, "cita", knjiga2, "i sara po margini.")
    semafor2.release()
    semafor3.acquire()
    print(ime, "cita", knjiga3, "i sara po margini.")
    semafor3.release()
    print("Ja sam", ime, "i vrato/la sam sve knjige.")

#t = ['t0', 't1', 't2', 't3', 't4', 't5', 't6'] # popravit od ovdje nadalje

for i in [t0, t1, t2, t3, t4, t5, t6]:
    i = threading.Thread(target=lektira, args=(ucenici[i],))
    
for i in range(0, 7):
    t[i].start()
