import threading, time

e = threading.Event()

def fun1():
    print("Funkc ce spavati 5 s, a zatim postaviti da se dogodio dogadjaj.")
    time.sleep(5)
    e.set()

def fun2():
    e.wait(3)
    print("Tko ceka, ne doceka.")

def fun3():
    e.wait()
    print("Tko ceka, doceka.")

t1 = threading.Thread(target=fun1)
t2 = threading.Thread(target=fun2)
t3 = threading.Thread(target=fun3)

t1.start()
t2.start()
t3.start()
