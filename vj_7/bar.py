import threading, time

b = threading.Barrier(3)

def funkcija(x):
    time.sleep(x)
    print("Nit koja ceka", x, "sekundi je dosla do bariere.")
    b.wait()
    print("Nit koja ceka", x, "sekundi je nastavila sa izvodjenjem.")

t1 = threading.Thread(target=funkcija, args=(3,))
t2 = threading.Thread(target=funkcija, args=(4,))
t3 = threading.Thread(target=funkcija, args=(2,))

t1.start()
t2.start()
t3.start()
