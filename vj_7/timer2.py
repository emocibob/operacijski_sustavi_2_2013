import threading

b = threading.Barrier(2)

def funkcija1():
    print("Voda spremna za caj.")
    b.wait()
    print("(...)")

def funkcija2():
    print("Kolacici madelaine taman kako treba.")
    b.wait()

caj = threading.Timer(3, funkcija1) # 60
kolac = threading.Timer(6, funkcija2) # 120

caj.start()
kolac.start()
