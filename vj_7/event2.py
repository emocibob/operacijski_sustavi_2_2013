import threading, time

e = threading.Event()

def odmrzavanje(n):
    time.sleep(n*60)
    print("Odmrzavanje gotovo.")
    e.set()

def pecnica(n):
    e.wait()
    time.sleep(n*60)
    print("Speceno.")

t1 = threading.Thread(target=odmrzavanje, args=(0.1,))
t2 = threading.Thread(target=pecnica, args=(0.1,))

t1.start()
t2.start()
