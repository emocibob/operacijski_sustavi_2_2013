import socket
import subprocess
import time

sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

sock.bind("mojsocket")

podatak1 = sock.recv(1024) # primi sa socketa 1024 B
print("Primljeno od klijenta: ", podatak1.decode())

podatak2 = sock.recv(1024) # primi sa socketa 1024 B
print("Primljeno od klijenta: ", podatak2.decode())

sock.close()

podatak3 = podatak1 + podatak2

sock2 = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

#sleep(2)

sock2.connect("mojsocket2")

sock2.send(podatak3) # encode?

sock2.close()

subprocess.call("rm mojsocket".split())
