a = "AndrewKoenig"
print("a = ", a)
print("a[1:5] = ", a[1:5])
print("a[:3] = ", a[:3])
print("a[:2] = ", a[:2])
print("a[-3] = ", a[-3])
print("a[:-1] = ", a[:-1])
print("a[1::2] = ", a[1::2])
print("a[::-1] = ", a[::-1])

b = list(a)
print(b)
