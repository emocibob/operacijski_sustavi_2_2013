print("Ispis neparnih brojeva od 1 do 101:\n")

for x in range(1,102):
    if (x % 2) != 0:
        print(x)

print("\n\nIspis brojeva djeljivih s 3 od 5 do 45:\n")

for x in range(5,46):
    if (x % 3) == 0:
        print(x)
