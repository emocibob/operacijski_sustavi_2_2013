x = -5

if x < 0:
  print("Negative")
elif x == 0:
  print("Zero")
else:
  print("Positive")


for x in range(2, 7):
  print(x, x**2)

x = 0
while x < 5:
  x += 1
  print(x, x*2)
