a="AndrewKoenig"

print(a, "\n")

print(a[1:5])
print(a[:3])
print(a[2:])
print(a[-3])
print(a[:-1])
print(a[1::2])
print(a[::-1])

print("")

b=list(a)

print(b, "\n")

print(b[1:5])
print(b[:3])
print(b[2:])
print(b[-3])
print(b[:-1])
print(b[1::2])
print(b[::-1])
